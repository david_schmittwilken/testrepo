/*
 * PIN.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */
#include "PIN.h"
#include "PORT.h"
#include "stdio.h"

	void PIN::SetPIN(uint8_t value_)
	{
		PIN::port.SetPin(this->pinNr, value_, 1);
	}
	uint8_t PIN::GetPIN()
	{
		return PIN::port.GetPin(this->pinNr, 1);
	}
	void PIN::TogglePIN()
	{
		PIN::port.TogglePIN();
	}

PIN::PIN(PORT port_, uint8_t pinNr_)
{
	port = port_;
	pinNr = pinNr_;
}



