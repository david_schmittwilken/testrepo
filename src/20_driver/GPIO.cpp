/*
 * GPIO.cpp
 *
 *  Created on: Oct 24, 2018
 *      Author: david
 */
#include "GPIO.h"

void GPIO::InitPin(uint8_t pinNr, PINCONFIGURATION pinConfig_){
	// Enable GPIO Peripheral clock
	switch ((uint32_t)gpio) {
	case (uint32_t)GPIOA_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
		break;
	case ((uint32_t)GPIOB_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
		break;
	case ((uint32_t)GPIOC_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
		break;
	case ((uint32_t)GPIOD_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
		break;
	case ((uint32_t)GPIOE_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
		break;
	case ((uint32_t)GPIOF_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
		break;
	case ((uint32_t)GPIOG_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
		break;
	case ((uint32_t)GPIOH_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
		break;
	case ((uint32_t)GPIOI_BASE):
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
		break;
	default:
		break;
	}
		/*reset all values for that pin and then set them*/
		/*reset*/
		gpio->MODER &= RESETREGATTR(MODE_MAX, 2, pinNr);
		gpio->OTYPER &= RESETREGATTR(OUTPUTTYPE_MAX, 1, pinNr);
		gpio->OSPEEDR &= RESETREGATTR(OSPEED_MAX, 2, pinNr);
		gpio->PUPDR &= RESETREGATTR(PULLUP_PULLDOWN_MAX, 2, pinNr);

		/*set*/
		gpio->MODER |= SETREGATTR(mode_, 2, pinNr);
		gpio->OTYPER |= SETREGATTR(outputType_, 2, pinNr);
		gpio->OSPEEDR |= SETREGATTR(VERY_HIGH_SPEED, 2, pinNr);
		gpio->PUPDR = SETREGATTR(pullUpPullDown_, 2, pinNr);
	}

	void GPIO::SetPin(uint8_t pinNr, uint8_t value, bool activeHigh)
	{
		/*TODO: add activehigh*/
		/*set value*/
		if(1 == value)
		{
				gpio->ODR |= SETREGATTR(SET_VALUE, 1, pinNr);
		}
		else
		/*reset value*/
			gpio->ODR &= RESETREGATTR(GPIOVALUE_MAX, 1, pinNr);
	}

	uint8_t GPIO::GetPin(uint8_t pinNr, bool activeHigh)
	{
		return ((gpio->IDR >> pinNr) & 1);
	}

	GPIO::GPIO(GPIO_TypeDef * gpio_)
	{
		gpio = gpio_;
	}


