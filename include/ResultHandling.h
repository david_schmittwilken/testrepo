/*
 * ResultHandling.h
 *
 *  Created on: 23.10.2018
 *      Author: dave
 */

#ifndef UTIL_RESULTHANDLING_H_
#define UTIL_RESULTHANDLING_H_

typedef enum RETURNTYPE
{
	RET_OK = 0,
	RET_NOT_SUPPORTED = 1,
	RET_ERROR = 1,
};


#endif /* UTIL_RESULTHANDLING_H_ */
